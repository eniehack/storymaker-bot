(import discord)
(import argparse)
(import random)
(import math)

(defn card-reversed? []
  (if (= (-> ((. random random)) (round)) 0)
      "reverse"
      ""))

(defn pick []
  (-> (, "知恵" "秩序" "理性" "清楚" "意志" "生命" "至誠" "節度" "善良" "誓約" "依頼" "創造" "調和" "解放" "寛容" "勇気" "厳格" "結合" "変化" "公式" "慈愛" "治癒" "庇護" "幸運")
      ((. random choice))))

(setv parser ((. argparse ArgumentParser) "a Discord bot for storymaker."))
((. parser add_argument) "-t" "--token" :help "discord bot token" :required True)
(setv args ((. parser parse_args)))

(setv client ((. discord Client)))

#@((. client event)
    (defn/a on_message [message]
      (if (= (. message author) (. client user))
          return)
      (if (in (. client user) (. message mentions))
          (do (setv arg ((. message content split)))
              (print (get arg 1))
              (if
                (= "generate" (get arg 1))
                ((await ((. message channel send) "以下のようなお題に沿った物語のあらすじを作成してください"))
                  (await ((. message channel send) f"主人公の過去: {(card-reversed?)} {(pick)}"))
                  (await ((. message channel send) f"主人公の現在: {(card-reversed?)} {(pick)}"))
                  (await ((. message channel send) f"援助者: {(card-reversed?)} {(pick)}"))
                  (await ((. message channel send) f"敵対者: {(card-reversed?)} {(pick)}"))
                  (await ((. message channel send) f"主人公の近未来: {(card-reversed?)} {(pick)}"))
                  (await ((. message channel send) f"結末・目的: {(card-reversed?)} {(pick)}"))))))))

((. client run) (. args token))
