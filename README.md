# storymaker-bot

大塚英志による創作指南書である、『物語の体操』に掲載されていたカードを用いたトレーニングをbot化したものです。

## install

Python 3.9と依存関係解決ツールpoetryを事前にインストールしておく必要があります。

```
git clone https://gitlab.com/eniehack/storymaker-bot.git
cd storymaker-bot
poetry install
```

## usage

### start

optionの `-t` でDiscordから取得したtokenを指定します。

```sh
poetry run hy ./timetrial_bot/main.hy  -t {discord bot token}
```


### discord usage

メンション先を`@storymaker-bot`としていますが、実際はDiscord側に登録した名前を入力することになります。

`@storymaker-bot generate`で実行されます。

